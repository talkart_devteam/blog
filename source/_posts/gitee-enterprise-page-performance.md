---
title: 码云企业版页面性能优化
tags: 性能优化
---

## 需求

红薯抱怨企业版页面没有那种梭一下就刷完的流畅度，加载完页面后还要等一段时间才完全呈现出内容。

## 分析

浏览器开发者工具统计的网络性能如下图所示：

{% asset_img network-performance.png '网络传输性能' %}

从图中可以看出：

- 仅 dashboard 页面就需要等 437ms 才能下载完成，由于该页面涉及到后端的模板渲染和相关代码，暂不优化它
- 最大文件的是 app.js，有 700KB，而且是经 gzip 压缩后的大小
- echarts.common.min.js 比 vendors_lib.js 小，但耗时却比它多

## 解决方案

### 优化 app.js 大小

app.js 作为企业版所有页面公用的基础 js 文件，包含了 jQuery、组件库、编辑器、图表、highlight.js、moment.js 等依赖库，其中编辑器、highlight.js、moment.js 这类体积大的依赖库在大部分页面中都用不到，可见有很大的优化空间。

在审查 app.js 的依赖后，得出的初步优化方法如下：

- 移除无用的图表库依赖
- 对于只在个别页面中使用的依赖库，改为按需引入
- 移除编辑器依赖，改为用 Webpack 打包，由相关组件使用 import() 方法按需异步加载

采用前两个方法进行优化后，app.js 大小变为 596KB，比优化前减少 104KB。

{% asset_img optimized-app-js-size.png '优化后的 app.js 大小' %}

### 优化 echarts.js 的传输性能

echarts.js 相比 enterprise_dashboard.js，下载速度慢了大约一倍，原因如下：

- assets.gitee.com 域名下的 enterprise_dashboard.js 有 CDN 加速，传输性能不会受到 gitee.com 主站性能影响
- enterprise_dashboard.js 有对应的 .gz 文件，服务器可直接使用，无需在每次请求时耗费时间对文件进行 gzip 压缩

webpack 打包的资源都托管在 assets.gitee.com 域名下，那么优化手段就是改用 webpack 打包 echarts。

经过优化后，echarts.js 的下载耗时已经减少到 vendors_lib.js 的一半：

{% asset_img optimized-echarts-js.png '优化后的 echarts.js 传输性能' %}
