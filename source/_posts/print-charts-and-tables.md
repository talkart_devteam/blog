---
title: 处理图表和表格的打印样式问题
date: 2020-02-14 17:19:12
tags:
    - 前端
    - 工作记录
---

新开发的统计页面需要有个打印功能，但在预览打印内容时出现了以下问题：

1. 样式缺失，内容排版混乱
1. ECharts 图表宽度溢出，导致文档中有滚动条
1. Element UI 的表格组件宽度未 100% 拉伸

对于第一个问题，原因是 `page_specific_style_bundle_tag` 方法输出的 link 标签的 media 属性默认值是 screen，这使得 link 标签引入的 css 代码仅在 screen 上有效。解决方法是手动将 media 属性设为 all，然后添加 `@media print` 查询，隐藏无关内容。

第二个问题，原因是在媒介切换为 print 时 ECharts 的图表尺寸仍然是用 screen 媒介下的尺寸，虽然有尝试过在 window 触发 beforeprint 事件时主动更新图表尺寸，但并没有什么用，后来根据搜索结果得知，可在打印前将图表转换为图片，并让 img 元素的宽度为 100% 以使图表宽度自适应。核心代码如下：

```js
function print() {
  const img = new Image()

  img.onload = () => {
    window.print()
  }
  img.src = this.chart.getDataURL()
  this.imgSrc = img.src
}
```

第三个问题，Element UI 的表格组件分别用三个 table 元素实现表头、表身和表尾，并使用 colgroup 元素来控制每一列的宽度，这些宽度是由 JavaScript 计算的，显然，在媒介切换为 print 时它并不会重新计算。解决方法是将表格组件中的三个 table 元素合并为一个，然后去除 colgroup 元素，让浏览器自动调整 table 布局。核心代码如下：

```js
export default {
  mounted() {
    $(window).on('beforeprint', () => {
      const $box = $(this.$el)
      const $parent = $box.parent()
      const $table = $($parent.find('.el-table__body-wrapper table')[0].outerHTML)

      $table.prepend($parent.find('.el-table__header-wrapper thead')[0].outerHTML)
      $table.append(`<tfoot>${$parent.find('.el-table__footer-wrapper tr')[0].outerHTML}</tfoot>`)
      $table.find('colgroup').remove()
      $box.empty().append($table)
    })
  }
}
```
