---
title: 前端代码目录结构调整方案
date: 2019-12-31 17:59:55
tags:
    - 重构
    - 前端
---

红薯提了一些关于码云前后端分离的问题，暗示码云现有前端代码需要改进，但以码云现有情况看来，前后端代码都混在同一个仓库里，共用同一开发流程，而且组件库中的部分组件依赖后端在页面中生成的 window.gon 对象，直接搞前后端分离的话难度非常大，因此，只能将这项工作拆分成多个步骤来进行，大致如下：

1. 调整目录结构
1. 整理 JavaScript 和 CSS 代码，使其不再依赖主仓库的老代码
1. 移动前端代码至新仓库中
1. 对企业版的页面逐一重构，实现前后端分离
1. 重构组件库，移除对 window.gon 对象的依赖

先从调整目录结构开始，让前端代码更容易从主仓库中分离出去，顺便处理掉老代码。

在上次的《{% post_link gitee-webpack-config-Improvement %}》中，部分前端代码已经整理进了 webpack 目录，这次对目录结构做进一步的调整，并把所有经过 webpack 打包的JavaScript 代码和 Vue 组件都移入 webpack 目录中。

调整后的目录里结构如下：

```text
webpack/
    packages/
        gitee-axios/
        gitee-framework/
        gitee-gantt-chart/
        gitee-locales/
        ...
    projects/
        api-doc/
        babel/
        community/
        enterprise/
            components/
            directives/
            lib/
            mixins/
            pages/
                dashboard/
                events/
                issues/
                members/
                milestones/
                programs/
                projects/
                pulls/
                wikis/
                ...
        mobile/
    shared/
        components/
        directives/
        lib/
        mixins/
    webpack.config.js
    webpack.projects.config.js
```

子项目移动到了 projects 目录，packages 和 shared 目录都包含被多个子项目依赖的代码，但 shared 包含的都是单个模块，而 packages 包含的是一些可作为依赖库/包的代码。
