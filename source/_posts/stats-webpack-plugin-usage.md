---
title: stats-webpack-plugin 插件的用法
date: 2020-05-13 10:51:17
tags:
    - Webpack
    - 插件
    - 前端
---

stats-webpack-plugin 插件能够生成资源清单文件，被用于码云的一些前端项目中，这个清单文件有两个用途：

1. 让服务器端能够根据 Webpack 配置的入口名称找到资源文件路径
1. 在线上环境出现问题需要回滚时，可以通过回退清单文件来快速将前端资源切换回上个版本，无需花时间重新打包构建前端资源

由于最近在处理资源清单文件合并问题时折腾了 stats-webpack-plugin 插件，所以顺便记录一些关于这个插件的用法。

基本用法：

```js
const StatsPlugin = require('stats-webpack-plugin')

module.exports = {
  plugins: [
    new StatsPlugin('stats.json', {
      chunkModules: true,
      exclude: [/node_modules[\\\/]react/]
    })
  ]
};
```

使用 cache 参数让多个 StatsPlugin() 插件实例将构建统计信息写入到同一 stats.json 文件：

```js
const options = {
  chunkModules: true,
  exclude: [/node_modules[\\/]/]
}

const cache = {}

module.exports = [
  {
    plugins: [
      new StatsPlugin('stats.json', options, cache)
    ]
  },
  {
    plugins: [
      new StatsPlugin('stats.json', options, cache)
    ]
  }
]
```

[gitee-vendor-dll](https://gitee.com/gitee-frontend/gitee-vendor-dll/commit/8da1b0f28ec78209866e31fc14331add0b72ba2f) 项目的 Webpack 配置是一个数组，按照原有的配置，Webpack 输出的 manifest.json 只会包含第一个配置的资源信息，正好之前在看 stats-webpack-plugin 插件的[测试程序](https://github.com/unindented/stats-webpack-plugin/blob/master/test/plugin.js)时发现了 cache 参数，所以用它解决了问题。
