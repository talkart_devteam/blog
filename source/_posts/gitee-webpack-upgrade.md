---
title: Webpack 4.x 升级记录
date: 2019-10-22 17:23:39
toc: true
tags:
    - webpack
    - 前端
---

## 主要改动

- 升级 webpack
- 升级 babel-loader
- 升级 vue-router
- 移除 vendors_lib 中的 lodash 依赖
- 从 webpack.base.config.js 中拆分出 webpack.babel.config.js，专用于打包的 js 文件
- 调整一些不能被 Tree Shaking 功能优化的模块的引入方式

## 问题记录

1. **Uncaught ReferenceError: vendors_lib is not defined**

    仓库新建、组织新建、企业开通等页面的 js 代码是用 webpack 打包的，只是为了用 ES6 语法，并不依赖 vendors_lib 里的代码，但 DllReferencePlugin 插件却判定为依赖 vendors_lib。

    对比 js 文件依赖信息和 vendors_lib 的 manifest.json 文件后发现有个共同依赖项: `webpack/buildin/global.js`，本打算用 scoped 模式引入依赖，但试了后一直报错 `Cannot resolve ...`，所以就放弃了。

    **解决方法：** 添加 webpack.babel.config.js 配置文件，不使用 DllReferencePlugin 插件。

1. **export 'xxxx' was not found in 'xxxx'**

    在 es 模块中用引入 commonjs 模块会出现这个问题，webpack 未能识别出模块导出的对象。用 `@babel/plugin-transform-modules-commonjs` 插件可以解决这个问题，但后来发现会影响 webpack 的 tree shaking 功能，所以就放弃用了。

    **解决方法：** 将 commonjs 模块改成 es 模块，也就是将 `module.exports = {}` 语句改成 `export {}`。

1. **Uncaught TypeError: Cannot assign to read only property 'exports' of object '#\<Object\>'**

    和上一个问题出自同一模块，这个模块引入了 `app/assets/javascripts/lib` 目录中的模块，而这个目录中的模块都是 ES5 兼容的，无需使用 babel-loader 转换也能用，转换后反而影响 webpack 编译。

    **解决方法：** 将该 `app/assets/javascripts/lib` 目录添加至 babel-loader 的 exclude 配置中。

1. **Failed to mount component: template or render function not defined**

    用 `import()` 加载组件返回的是 Module 类型的对象，`module.default` 里才是组件的描述对象。一开始怀疑是 Webpack 的问题，但看了打包输出的代码后感觉也没什么问题，后来找到了[相关文档](https://webpack.js.org/guides/code-splitting/#dynamic-imports)，里面有这么一段：

    > The reason we need default is that since webpack 4, when importing a CommonJS module, the import will no longer resolve to the value of module.exports, it will instead create an artificial namespace object for the CommonJS module. For more information on the reason behind this, read webpack 4: import() and CommonJs

    所以只能是 Vue Router 的问题了，原因应该是现在用的 2.3.1 版本没有对 Module 类型对象做处理。Vue Router 在 GitHub 上的发行记录中的最新版本是 3.1.3，翻了历史更新日志后发现这个问题已经在 2.8.0 版本中得到解决，相关描述如下：

    > Properly resolve async components imported from native ES modules ([8a28426](https://github.com/vuejs/vue-router/commit/8a28426f3aba3cae6c85ecdc80673de048d53b40))

    **解决方法：** 升级 Vue Router 到最新版。

1. **Uncaught ReferenceError: _MessageBox is not defined**

    问题代码：

    ```js
    Vue.prototype.$msgbox = _messageBox2.default;
    Vue.prototype.$alert = _MessageBox.alert;
    Vue.prototype.$confirm = _MessageBox.confirm;
    Vue.prototype.$prompt = _MessageBox.prompt;
    Vue.prototype.$message = _message2.default;
    Vue.prototype.$notify = _notification2.default;
    ```
    源代码是这样的：

    ```js
    Vue.prototype.$msgbox = MessageBox
    Vue.prototype.$alert = MessageBox.alert
    Vue.prototype.$confirm = MessageBox.confirm
    Vue.prototype.$prompt = MessageBox.prompt
    Vue.prototype.$message = Message
    Vue.prototype.$notify = Notification
    ```

    这也能出问题？第一次用时能解析为 `_messageBox2.default`，后面用时却全解析为 `_MessageBox`。经测试发现是 [babel-plugin-component](https://github.com/ElementUI/babel-plugin-component) 的问题，这个插件很久没更新了，就算反馈问题也不一定会被处理，不过这个插件是 fork 自 [babel-plugin-import](https://github.com/ant-design/babel-plugin-import) 的，可以改用它。

    **解决方法：** 改用 babel-plugin-import，重命名 .babelrc 为 .babelrc.js，然后将插件配置改为：

    ```js
    [
      "import",
      {
        "libraryName": "osc-element-ui",
        "style": (name) => {
          return `osc-element-ui/lib/theme-chalk/${name.substr(name.lastIndexOf('/') + 1)}.css`
        }
      },
      "osc-element-ui"
    ],
    ```

1. **missing param for named route "project#tree": Expected "0" to be defined**

    Vue Router 在 3.0.2 版本中已经将 `*` 通配符的参数名从 0 改为 pathMatch。

    > Give * routes a param name (instead of 0) ([#1994](https://github.com/vuejs/vue-router/issues/1994)) [#1995](https://github.com/vuejs/vue-router/pull/1995) @posva

    **解决方法：** 将路由参数中的 0 改成 pathMatch

1. **Uncaught (in promise) Navigating to current location ("/xxxx") is not allowed**

    菜单栏点击链接时调用了一次 `$router.push()`，目标页面在初始化后会调用 `$router.replace()` 更新 query 参数，这时路由没有变化，所以会报错。

    **解决方法：** 给 `$router.replace()` 添加异常捕获，也就是改成：

    ```js
    this.$router.replace({ query }).catch(err => err)`
